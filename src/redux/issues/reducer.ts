import {
    Actions, CLEAN_LOADING, CLEAN_UPDATE, CREATE_ISSUE,
    GET,
    GET_LIST,
    GO_TO_PAGE,
    LOADING,
    RELOAD_LIST,
    UNDO_GET,
    UNDO_GET_LIST,
    UPDATE_ISSUE,
    UPDATING
} from 'src/redux/issues/actions';
import { Issues } from 'src/redux/issues/state';

export default (state: Issues, action: Actions): Issues => {
    switch(action.type) {
        case GO_TO_PAGE: {
            return {
                ...state,
                list: {
                    ...state.list,
                    currentPage: Math.max(0, Math.min(action.page, state.list.totalPages! - 1))
                }
            };
        }

        case GET_LIST: {
            const pages = [...state.list.pages];
            pages[action.page - 1] = action.values;
            return {
                ...state,
                list: {
                    ...state.list,
                    currentPage: action.page - 1,
                    loading: false,
                    pages,
                    totalPages: action.totalPages,
                }
            };
        }

        case LOADING: {
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                }
            };
        }

        case UNDO_GET_LIST: {
            return {
                ...state,
                list: {
                    currentPage: undefined,
                    loading: false,
                    pages: [],
                }
            };
        }

        case RELOAD_LIST: {
            return {
                ...state,
                list: {
                    currentPage: 0,
                    loading: true,
                    pages: []
                }
            };
        }

        case UPDATING: {
            return {
                ...state,
                updating: true,
            };
        }

        case UPDATE_ISSUE: {
            if(state.issue === null) {
                const pages = [...state.list.pages];
                let p = 0;
                let i: number | null = null;
                for(p = 0; p < pages.length; p++) {
                    i = pages[ state.list.currentPage! ].findIndex((issue) => issue.id_parte_informatica === action.issue.id_parte_informatica);
                    if(i === -1) {
                        i = null;
                    } else {
                        break;
                    }
                }

                if(i !== null) {
                    pages[p][ i ] = action.issue;
                    return {
                        ...state,
                        list: {
                            ...state.list,
                            pages,
                        },
                        updating: false,
                    };
                } else {
                    return state;
                }
            } else {
                return {
                    ...state,
                    issue: { ...state.issue, ...action.issue },
                    updating: false,
                };
            }
        }

        case GET: {
            return {
                ...state,
                issue: action.issue,
                issueCreator: action.issueCreator,
                list: {
                    ...state.list,
                    loading: false,
                }
            };
        }

        case UNDO_GET: {
            return {
                ...state,
                issue: null,
                issueCreator: null,
            };
        }

        case CREATE_ISSUE: {
            return {
                ...state,
                issue: action.issue,
                issueCreator: action.issueCreator,
                updating: false,
            };
        }

        case CLEAN_LOADING: {
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false,
                }
            };
        }

        case CLEAN_UPDATE: {
            return {
                ...state,
                updating: false,
            };
        }
    }

    return state || {
        issue: null,
        issueCreator:null,
        list: { loading: false, pages: [] },
        updating: false,
    };
};