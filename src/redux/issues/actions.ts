import { Action, Dispatch } from 'redux';

import {
    ComputerIssue,
    ComputerIssues,
    ComputerIssueState,
    NewComputerIssue,
    SmallComputerIssue,
    User,
    Users
} from 'src/common/lib/api';
import { catchErrors as f } from 'src/common/redux/errors/decorator';

export const GET_LIST = 'issues:GET_LIST';
export const UNDO_GET_LIST = 'issues:UNDO_GET_LIST';
export const GET = 'issues:GET';
export const UNDO_GET = 'issue:UNDO_GET';
export const CREATE_ISSUE = 'issues:CREATE_ISSUE';
export const UPDATE_ISSUE = 'issues:UPDATE_ISSUE';
export const GO_TO_PAGE = 'issues:GO_TO_PAGE';
export const LOADING = 'issues:LOADING';
export const RELOAD_LIST = 'issues:RELOAD_LIST';
export const UPDATING = 'issues:UPDATING';
export const CLEAN_UPDATE = 'issues:CLEAN_UPDATE';
export const CLEAN_LOADING= 'issues:CLEAN_LOADING';

interface GetListAction extends Action<typeof GET_LIST> {
    page: number;
    totalPages: number;
    values: SmallComputerIssue[];
}

type UndoGetListAction = Action<typeof UNDO_GET_LIST>;

interface GoToPageAction extends Action<typeof GO_TO_PAGE> {
    page: number;
}

type LoadingAction = Action<typeof LOADING>;

type ReloadListAction = Action<typeof RELOAD_LIST>;

type UpdatingAction = Action<typeof UPDATING>;

interface UpdateIssueAction extends Action<typeof UPDATE_ISSUE> {
    issue: SmallComputerIssue;
}

interface GetAction extends Action<typeof GET> {
    issue: ComputerIssue;
    issueCreator: User;
}

type UndoGetAction = Action<typeof UNDO_GET>;

interface CreateAction extends Action<typeof CREATE_ISSUE> {
    issue: ComputerIssue;
    issueCreator: User;
}

type CleanUpdateAction = Action<typeof CLEAN_UPDATE>;

type CleanLoadingAction = Action<typeof CLEAN_LOADING>;

export type Actions = GetListAction | UndoGetListAction | GoToPageAction | LoadingAction | ReloadListAction |
    UpdateIssueAction | UpdatingAction | GetAction | UndoGetAction | CreateAction | CleanUpdateAction |
    CleanLoadingAction;


export const getList = (page: number, opts: { fromDate?: Date, toDate?: Date, getAllItems?: boolean })  => f(async (dispatch: Dispatch) => {
    dispatch({ type: LOADING });
    const values = await ComputerIssues.getList(page + 1, opts);
    dispatch({
        page: page + 1,
        totalPages: values.pages,
        type: GET_LIST,
        values: values.results,
    });
});

export const reloadList = (opts: { fromDate?: Date, toDate?: Date, getAllItems?: boolean }) => f(async (dispatch: Dispatch) => {
    dispatch({ type: RELOAD_LIST });
    getList(0, opts)(dispatch);
});

export const undoGetList = (): UndoGetListAction => ({
    type: UNDO_GET_LIST,
});

export const goToPage = (page: number): GoToPageAction => ({
    page,
    type: GO_TO_PAGE,
});

export const discardIssue = (issue: SmallComputerIssue, reason: string) => f(async (dispatch: Dispatch) => {
    dispatch({ type: UPDATING });
    const newIssue = await ComputerIssues.discard(issue.id_parte_informatica, reason);
    dispatch({
        issue: newIssue,
        type: UPDATE_ISSUE,
    });
});

export const loadIssue = (issueId: number) => f(async (dispatch: Dispatch) => {
    dispatch({ type: LOADING });
    const issue = await ComputerIssues.getOne(issueId);
    const user = await Users.getUser(String(issue.usuario_niu));
    dispatch({
        issue,
        issueCreator: user,
        type: GET
    });
});

export const removeLoadedIssue = () => ({
    type: UNDO_GET
});

export const updateIssueState = (issue: ComputerIssue, reason: string, state?: ComputerIssueState) => f(async (dispatch: Dispatch) => {
    dispatch({ type: UPDATING });
    const newIssue = await ComputerIssues.modifyState(issue, reason, state);
    dispatch({
        issue: newIssue,
        type: UPDATE_ISSUE,
    });
});

export const updateIssueState2 = (issue: ComputerIssue, reason: string) => f(async (dispatch: Dispatch) => {
    dispatch({ type: UPDATING });
    const newIssue = await ComputerIssues.sendWaitingUserResponse(issue, reason);
    dispatch({
        issue: newIssue,
        type: UPDATE_ISSUE,
    });
});

export const createIssue = (issue: NewComputerIssue) => f(async (dispatch: Dispatch) => {
    dispatch({ type: UPDATING });
    const newIssue = await ComputerIssues.create(issue);
    const user = await Users.getUser(String(newIssue.usuario_niu));
    dispatch({
        issue: newIssue,
        issueCreator: user,
        type: CREATE_ISSUE,
    });
});

export const cleanUpdateStatus = () => ({
    type: CLEAN_UPDATE,
});

export const cleanLoadingStatus = () => ({
    type: CLEAN_LOADING,
});
