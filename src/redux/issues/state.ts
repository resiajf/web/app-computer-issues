import { ComputerIssue, SmallComputerIssue, User } from 'src/common/lib/api';

export interface Issues {
    list: {
        loading: boolean;
        totalPages?: number;
        pages: SmallComputerIssue[][];
        currentPage?: number;
    };
    issue: ComputerIssue | null;
    issueCreator: User | null;
    updating: boolean;
}