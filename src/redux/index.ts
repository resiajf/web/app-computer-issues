import { combineReducers } from 'redux';

import { reducers as commonReducers, State as CommonState } from 'src/common/redux';
import { default as issues } from 'src/redux/issues/reducer';
import { Issues } from 'src/redux/issues/state';

export const reducers = combineReducers<State>({
    ...commonReducers,
    issues,
});

// tslint:disable-next-line:no-empty-interface
export interface State extends CommonState {
    issues: Issues;
}
