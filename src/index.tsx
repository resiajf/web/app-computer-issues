import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import ErrorPreLoadingPage from 'src/common/components/error-pre-loading';
import App from 'src/common/containers/app';
import i18n from 'src/common/lib/i18n';
import { Session } from 'src/common/lib/session';
import { moduleName } from 'src/routes';

(async () => {
    try {
        if(process.env.NODE_ENV !== 'production') {
            //Redirects to the correct path (only in debug)
            if(!window.location.pathname.startsWith(`/${moduleName}`)) {
                window.location.assign(`/${moduleName}`);
            }
        }

        const user = Session.convertToStatusUser(await Session.checkSession(), moduleName);

        require('bootstrap');
        require('./styles/styles.css');

        const composeUpdated = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;
        const store = createStore(
            require('./redux').reducers,
            { user }, //Initial state
            composeUpdated(applyMiddleware(thunk))
        );

        ReactDOM.render(
            <Provider store={store}>
                <BrowserRouter basename={ `/${moduleName}` }>
                    <I18nextProvider i18n={ i18n }>
                        <App />
                    </I18nextProvider>
                </BrowserRouter>
            </Provider>,
            document.getElementById('root') as HTMLElement
        );
        registerServiceWorker();
    } catch(e) {
        if(e === null) {
            Session.logIn();
        }
        console.error(e);
        ReactDOM.render(
            <ErrorPreLoadingPage error={ e } />,
            document.getElementById('root') as HTMLElement
        );
    }
})();
