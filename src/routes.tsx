import React from 'react';
import { Redirect } from 'react-router';

import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

export const moduleName = 'computer-issues';

const UserIssuesListPage = asyncComponent(() => import('./containers/user-issues-list-page'));
const AdminIssuesListPage = asyncComponent(() => import('./containers/admin-issues-list-page'));
const IssuePage = asyncComponent(() => import('./containers/issue-page'));
const NewIssuePage = asyncComponent(() => import('./containers/new-issue-page'));

export const routes: Array<Route<any, any>> = [
    route('/', 'home', () => <Redirect to="/1" />, 'home', { exact: true }),
    route('/:from(from:\\d+/)?:to(to:\\d+/)?:page(\\d+)', 'home', UserIssuesListPage, 'fullHome', { hide: true }),
    route('/issue/:id(\\d+)', 'issue', IssuePage, 'issue', { hide: true }),
    route('/admin', 'control', () => <Redirect to="/admin/1" />, 'control', { exact: true }),
    route('/admin/:from(from:\\d+/)?:to(to:\\d+/)?:page(\\d+)', 'control', AdminIssuesListPage, 'fullControl', { hide: true }),
    route('/create', 'create', NewIssuePage, 'create'),
];