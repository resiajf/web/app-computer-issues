import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { clearError } from 'src/common/redux/errors/actions';
import { ErrorsState } from 'src/common/redux/errors/state';
import { NewIssuePage } from 'src/components/pages/new-issue-page';
import {
    NewIssuePageDispatchToProps,
    NewIssuePageStateToProps
} from 'src/components/pages/new-issue-page.interface';
import { State } from 'src/redux';
import { cleanUpdateStatus, createIssue } from 'src/redux/issues/actions';

const mapStateToProps = ({ issues, user, errors }: State): NewIssuePageStateToProps => ({
    invalidError: errors.invalid,
    issueNum: issues.issue ? issues.issue.id_parte_informatica : null,
    serverError: errors.serverUnavailable,
    updating: issues.updating,
    user,
});

const mapDispatchToProps = (dispatch: any): NewIssuePageDispatchToProps => ({
    cleanUpdateStatus: () => dispatch(cleanUpdateStatus()),
    clearError: (what?: keyof ErrorsState) => dispatch(clearError(what)),
    create: (issue) => dispatch(createIssue(issue)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewIssuePage));
