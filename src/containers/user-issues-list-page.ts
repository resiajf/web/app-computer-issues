import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { clearError } from 'src/common/redux/errors/actions';
import { ErrorsState } from 'src/common/redux/errors/state';
import { IssuesListPage } from 'src/components/pages/issues-list-page';
import {
    IssuesListPageDispatchToProps,
    IssuesListPageStateToProps
} from 'src/components/pages/issues-list-page.interface';
import { State } from 'src/redux';
import { cleanUpdateStatus, discardIssue, getList, goToPage, reloadList, undoGetList } from 'src/redux/issues/actions';

const mapStateToProps = ({ errors, issues, user }: State): IssuesListPageStateToProps => ({
    currentPage: issues.list.currentPage,
    issues: issues.list.pages ? issues.list.pages[issues.list.currentPage || 0] : undefined,
    loading: issues.list.loading,
    notFoundError: errors.notFound,
    serverError: errors.serverUnavailable,
    totalPages: issues.list.totalPages,
    updating: issues.updating,
    user,
});

const mapDispatchToProps = (dispatch: any): IssuesListPageDispatchToProps => ({
    clearError: (what?: keyof ErrorsState) => dispatch(clearError(what)),
    clearUpdating: () => dispatch(cleanUpdateStatus()),
    discardIssue: (issue, reason) => dispatch(discardIssue(issue, reason)),
    goToPage: (page: number) => dispatch(goToPage(page)),
    loadPage: (page: number, fromDate?: Date, toDate?: Date) => dispatch(getList(page, { fromDate, toDate })),
    reloadList: (fromDate?: Date, toDate?: Date) => dispatch(reloadList({ fromDate, toDate })),
    unload: () => dispatch(undoGetList()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IssuesListPage));
