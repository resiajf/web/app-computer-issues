import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { clearError } from 'src/common/redux/errors/actions';
import { ErrorsState } from 'src/common/redux/errors/state';
import { IssuePage } from 'src/components/pages/issue-page';
import {
    IssuePageDispatchToProps,
    IssuePageStateToProps
} from 'src/components/pages/issue-page.interface';
import { State } from 'src/redux';
import { cleanLoadingStatus, cleanUpdateStatus, discardIssue, loadIssue, removeLoadedIssue, updateIssueState, updateIssueState2 } from 'src/redux/issues/actions';

const mapStateToProps = ({ issues, errors, user }: State): IssuePageStateToProps => ({
    issue: issues.issue,
    issueCreator: issues.issueCreator,
    loading: issues.list.loading,
    notFoundError: errors.notFound,
    serverError: errors.serverUnavailable,
    updating: issues.updating,
    user,
});

const mapDispatchToProps = (dispatch: any): IssuePageDispatchToProps => ({
    clearError: (what?: keyof ErrorsState) => dispatch(clearError(what)),
    clearLoading: () => dispatch(cleanLoadingStatus()),
    clearUpdating: () => dispatch(cleanUpdateStatus()),
    discardIssue: (issue, reason) => dispatch(discardIssue(issue, reason)),
    loadIssue: (issue) => dispatch(loadIssue(issue)),
    unloadIssue: () => dispatch(removeLoadedIssue()),
    updateStatus: (issue, reason, state) => dispatch(updateIssueState(issue, reason, state)),
    updateUserStatus: (issue, reason) => dispatch(updateIssueState2(issue, reason)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IssuePage));
