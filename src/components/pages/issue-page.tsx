import moment from 'moment';
import React from 'react';
import { Trans, translate } from 'react-i18next';

import asyncComponent from 'src/common/components/async-component';
import { Badge } from 'src/common/components/bootstrap/badge';
import { Button } from 'src/common/components/bootstrap/buttons';
import { ComputerIssueState } from 'src/common/lib/api';
import { AvailabilityChart } from 'src/components/availability-chart';
import { ComputerIssueStateBadge } from 'src/components/computer-issue-state-badge';
import { DiscardIssueModal } from 'src/components/discard-issue-modal';
import { IssuePageProps } from 'src/components/pages/issue-page.interface';
import { TrackingItem } from 'src/components/tracking-item';

const ChangeStateForm = asyncComponent(() => import('src/components/change-state-form'));

interface IssuePageState {
    showModal: boolean;
    hasAdmin: boolean;
    notFoundError: string | null;
}

class IssuePageClass extends React.Component<IssuePageProps, IssuePageState> {

    private isDiscarding: boolean = false;

    constructor(props: IssuePageProps) {
        super(props);

        this.state = {
            hasAdmin: false,
            notFoundError: null,
            showModal: false,
        };

        this.onDiscardModalDoDiscard = this.onDiscardModalDoDiscard.bind(this);
        this.onDiscardModalDismiss = this.onDiscardModalDismiss.bind(this);
        this.onUpdateUserStatus = this.onUpdateUserStatus.bind(this);
        this.onUpdateStatus = this.onUpdateStatus.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    public componentDidMount() {
        if(this.props.issue === null) {
            //Load only if there's no issue
            this.props.loadIssue(Number(this.props.match.params.id));
        }
        this.setState({
            hasAdmin: this.props.user.permissionsPerPage['issue'].indexOf('all') !== -1,
        });
    }

    public componentDidUpdate(prevProps: Readonly<IssuePageProps>) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            this.props.loadIssue(Number(this.props.match.params.id));
            this.setState({
                notFoundError: null,
                showModal: false,
            });
        }

        if(prevProps.updating && !this.props.updating && !this.props.notFoundError && !this.props.serverError) {
            if(this.isDiscarding) {
                this.isDiscarding = false;
                this.setState({
                    showModal: false
                });
            }
        }

        if(!prevProps.notFoundError && this.props.notFoundError) {
            if(this.props.loading) {
                this.props.clearLoading();
            }
            if(this.props.updating) {
                this.props.clearUpdating();
            }
            this.setState({
                notFoundError: this.props.t(this.props.notFoundError.translatedMessageKey),
            });
            setTimeout(() => this.props.clearError('notFound'));
        }

        if(!prevProps.serverError && this.props.serverError) {
            if(this.props.loading) {
                this.props.clearLoading();
            }
            if(this.props.updating) {
                this.props.clearUpdating();
            }
            setTimeout(() => this.props.clearError('serverUnavailable'));
        }
    }

    public componentWillUnmount() {
        this.props.unloadIssue();
    }

    public render() {
        const { history, issue, issueCreator, loading, user, t } = this.props;

        if(loading) {
            return (
                <div>
                    <h1 className="display-4">
                        { t('item.loading') }
                    </h1>
                </div>
            );
        } else if(!issue) {
            //TODO handle error
            return (
                <div>
                    <h1 className="display-4">
                        <Trans i18nKey="item.not-found">
                            Issue #{{ number: this.props.match.params.id }}
                        </Trans>
                    </h1>
                    <p className="lead text-muted">{ this.state.notFoundError! }</p>
                </div>
            );
        }

        return (
            <div>
                <h1 className="display-4">
                    <Trans i18nKey="item.title">
                        Issue #{{ number: issue.id_parte_informatica }}
                    </Trans>
                </h1>
                <div><small>{ issueCreator!.display_name } - Apt. { issueCreator!.apartment }</small></div>

                <ComputerIssueStateBadge state={ issue.state } t={ t } />
                { issue.is_wifi && <Badge type="info" className="ml-1">{ t('issue.is_wifi') }</Badge> }
                { issue.is_wire && <Badge type="info" className="ml-1">{ t('issue.is_wire') }</Badge> }
                { issue.is_other && <Badge type="info" className="ml-1">{ t('issue.is_other') }</Badge> }

                <p className="lead">
                    { issue.issue_description }
                </p>

                <div className="text-right mt-1 mb-2">
                    <a href="#" className="btn btn-outline-primary btn-sm mr-1" onClick={history.goBack}>{ t('item.back_to_list') }</a>
                    { issue.state !== 'discarded' && issue.state !== 'solved' && issue.state !== 'wont-fix' && issue.usuario_niu === user.niu &&
                        <Button type="danger"
                                outline={ true }
                                size="sm"
                                onClick={ this.toggleModal }>{ t('issue.discard.discard') }</Button> }
                </div>

                { this.state.hasAdmin && issue.state !== 'discarded' && issue.state !== 'solved' &&
                    issue.state !== 'wont-fix' && issue.state !== 'waiting-user' &&
                    <ChangeStateForm onUpdateStatus={ this.onUpdateStatus } updating={ this.props.updating } /> }

                { !this.state.hasAdmin && issue.usuario_niu === user.niu && issue.state === 'waiting-user' &&
                    <ChangeStateForm onUpdateStatus={ this.onUpdateUserStatus } updating={ this.props.updating }
                                     forceStatusChangeTo="waiting" /> }

                <div className="mb-4">
                    <h2>{ t('item.availability_title') }</h2>
                    <AvailabilityChart availability={ issue!.availability_json } />
                </div>

                <div className="row">
                    <div className="col-12">
                        <h2>{ t('item.tracking_title') }</h2>
                    </div>
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header">
                                <ComputerIssueStateBadge state="pending" t={ t }/>
                            </div>
                            <div className="card-body">
                                <Trans i18nKey="item.created_at">
                                    Created at <time dateTime={ issue.fecha_creacion.toString() }>
                                        {{ time: moment.utc(issue.fecha_creacion).format('LLL') }}
                                    </time>
                                </Trans>
                            </div>
                        </div>
                    </div>

                    { issue.tracking.map((item, i) => <TrackingItem key={ i }
                                                                    item={ item }
                                                                    nextState={ i < issue.tracking.length - 1 ? issue.tracking[i + 1].from_state : issue.state } />) }
                </div>

                <DiscardIssueModal onDiscard={ this.onDiscardModalDoDiscard }
                                   onDismiss={ this.onDiscardModalDismiss }
                                   show={ this.state.showModal } />
            </div>
        );
    }

    private toggleModal(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.setState({
            showModal: !this.state.showModal,
        });
    }

    private onDiscardModalDoDiscard(reason: string) {
        this.isDiscarding = true;
        this.props.discardIssue(this.props.issue!, reason);
    }

    private onDiscardModalDismiss() {
        this.setState({
            showModal: false,
        });
    }

    private onUpdateStatus(reason: string, state?: ComputerIssueState) {
        this.props.updateStatus(
            this.props.issue!,
            reason,
            state
        );
    }

    private onUpdateUserStatus(reason: string) {
        this.props.updateUserStatus(
            this.props.issue!,
            reason,
        );
    }

}

export const IssuePage = translate()(IssuePageClass);
