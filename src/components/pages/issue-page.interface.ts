import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import {
    ComputerIssue,
    ComputerIssueState,
    NotFoundError,
    ServerUnavailableError,
    User as ApiUser
} from 'src/common/lib/api';
import { ErrorsState } from 'src/common/redux/errors/state';
import { User } from 'src/common/redux/user/state';

export interface IssuePageStateToProps {
    loading: boolean;
    issue: ComputerIssue | null;
    issueCreator: ApiUser | null;
    updating: boolean;
    notFoundError: NotFoundError | null;
    serverError: ServerUnavailableError | null;
    user: User;
}

export interface IssuePageDispatchToProps {
    clearError: (what?: keyof ErrorsState) => void;
    clearLoading: () => void;
    clearUpdating: () => void;
    discardIssue: (issue: ComputerIssue, reason: string) => void;
    loadIssue: (issueId: number) => void;
    unloadIssue: () => void;
    updateStatus: (issue: ComputerIssue, reason: string, state?: ComputerIssueState) => void;
    updateUserStatus: (issue: ComputerIssue, reason: string) => void;
}

export type IssuePageOwnProps = RouteComponentProps<{ id: string }>;

export type IssuePageProps = IssuePageStateToProps & IssuePageDispatchToProps & IssuePageOwnProps
    & InjectedTranslateProps;