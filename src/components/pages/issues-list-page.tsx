import moment from 'moment';
import React from 'react';
import Datetime from 'react-datetime';
import { translate } from 'react-i18next';
import { Link } from 'react-router-dom';

import { Button } from 'src/common/components/bootstrap/buttons';
import LoadingSpinner from 'src/common/components/load-spinner';
import Pagination from 'src/common/components/pagination';
import { SmallComputerIssue } from 'src/common/lib/api';
import ComputerIssueItem from 'src/components/computer-issue-item';
import { DiscardIssueModal } from 'src/components/discard-issue-modal';
import { IssuesListPageProps } from 'src/components/pages/issues-list-page.interface';

import 'react-datetime/css/react-datetime.css';
import { toast } from 'react-toastify';

interface IssuesListPageState {
    fromDate: moment.Moment | undefined;
    toDate: moment.Moment;
    modalIssue: SmallComputerIssue | null;
}

class IssuesListPageClass extends React.Component<IssuesListPageProps, IssuesListPageState> {

    private waitingForDiscardUpdate: boolean = false;

    constructor(props: IssuesListPageProps) {
        super(props);

        this.state = {
            fromDate: undefined,
            modalIssue: null,
            toDate: moment().startOf('day'),
        };

        this.onDiscardModalDoDiscard = this.onDiscardModalDoDiscard.bind(this);
        this.onDiscardModalDismiss = this.onDiscardModalDismiss.bind(this);
        this.fromDateChanged = this.fromDateChanged.bind(this);
        this.toDateChanged = this.toDateChanged.bind(this);
        this.onShowModal = this.onShowModal.bind(this);
        this.clearFilter = this.clearFilter.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.goToPage = this.goToPage.bind(this);
    }

    public componentDidMount() {
        //We add one day to toDate to grab all the results from that day too
        this.props.loadPage(
            this.page,
            this.fromDate ? this.fromDate.toDate() : undefined,
            this.toDate ? this.toDate.add(1, 'day').toDate() : undefined
        );
        this.setState({
            fromDate: this.fromDate,
            toDate: this.toDate || moment().startOf('day'),
        });
    }

    public componentWillUnmount() {
        this.props.unload();
    }

    public componentDidUpdate(prevProps: Readonly<IssuesListPageProps>) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            if(this.toDate) {
                this.props.loadPage(
                    this.page,
                    this.fromDate ? this.fromDate.toDate() : undefined,
                    this.toDate.add(1, 'day').toDate());
                this.setState({
                    fromDate: this.fromDate,
                    toDate: this.toDate,
                });
            } else {
                this.props.loadPage(this.page);
                this.setState({
                    fromDate: undefined,
                    toDate: moment().startOf('day'),
                });
            }
        }

        if(prevProps.updating && !this.props.updating && !this.props.notFoundError && !this.props.serverError) {
            if(this.waitingForDiscardUpdate) {
                this.waitingForDiscardUpdate = false;
                this.setState({
                    modalIssue: null,
                });
            }
        }

        if(!prevProps.notFoundError && this.props.notFoundError) {
            if(this.props.updating) {
                this.props.clearUpdating();
                if(this.waitingForDiscardUpdate) {
                    this.waitingForDiscardUpdate = false;
                    toast.error(<p>
                        { this.props.t('issue.discard.could_not_discard') }<br/>
                        <small>{ this.props.t(this.props.notFoundError.translatedMessageKey) }</small>
                    </p>);
                }
            }
            setTimeout(this.props.clearError);
        }

        if(!prevProps.serverError && this.props.serverError) {
            if(this.props.updating) {
                this.props.clearUpdating();
            }
            setTimeout(() => this.props.clearError('serverUnavailable'));
        }
    }

    public render() {
        const { user, t } = this.props;
        const page = this.page;

        let content;
        let showFilter = true;
        if(this.props.loading && !this.props.issues) {
            content = (
                <div>
                    <LoadingSpinner />
                </div>
            );
        } else if(!this.props.loading && (!this.props.issues || this.props.issues.length === 0)) {
            if(this.props.totalPages !== undefined && this.page > this.props.totalPages) {
                setTimeout(() => this.goToPage(this.props.totalPages! - 1));
                content = '...';
            } else if(this.toDate) {
                content = (
                    <div className="text-center">
                        <p className="lead">
                            { t('issue.no_results') }
                        </p>
                    </div>
                );
            } else {
                showFilter = false;
                content = (
                    <div className="text-center">
                        <p className="lead">
                            {t('issue.no_issues')}
                        </p>
                        <Link to="/create" className="btn btn-outline-primary">{t('issue.no_issues_link')}</Link>
                    </div>
                );
            }
        } else {
            content = (
                <div>
                    <Pagination page={ page } pages={ this.props.totalPages! } onChange={ this.goToPage } />
                    <div className="row">
                        { (this.props.issues || [])
                            .map((issue, i) => <ComputerIssueItem issue={ issue } key={i} userNiu={ user.niu } onShowModal={ this.onShowModal } />) }
                    </div>
                    <Pagination page={ page } pages={ this.props.totalPages! } onChange={ this.goToPage } />
                </div>
            );
        }

        const beforeNow = (currentDate: any) => currentDate.isBefore(moment());
        const beforeToDate = (currentDate: any) => currentDate.isBefore(this.state.toDate);

        return (
            <div>
                { showFilter && <div className="form-inline mb-4">
                    <span>{ t('issue.from') }</span>

                    <label className="sr-only" htmlFor="from-date">{ t('issue.from_date') }</label>
                    <Datetime timeFormat={ false }
                              className="ml-2 mr-2"
                              value={ this.state.fromDate }
                              isValidDate={ beforeToDate }
                              onChange={ this.fromDateChanged }
                              inputProps={{
                                  className: 'form-control form-control-sm',
                                  id: 'from-date',
                                  placeholder: t('issue.no_from_date'),
                              }} />

                    <span>{ t('issue.to') }</span>

                    <label className="sr-only" htmlFor="to-date">{ t('issue.to_date') }</label>
                    <Datetime timeFormat={ false }
                              className="ml-2 mr-2"
                              value={ this.state.toDate }
                              isValidDate={ beforeNow }
                              onChange={ this.toDateChanged }
                              inputProps={{
                                  className: 'form-control form-control-sm',
                                  id: 'to-date',
                              }} />

                    <Button type="primary" size="sm" className="mr-2" onClick={ this.applyFilter }>{ t('issue.apply_filter') }</Button>
                    <Button type="secondary" size="sm" disabled={ !this.toDate } onClick={ this.clearFilter }>{ t('issue.clear_filter') }</Button>
                </div> }

                { content }

                <DiscardIssueModal onDiscard={ this.onDiscardModalDoDiscard }
                                   onDismiss={ this.onDiscardModalDismiss }
                                   show={ !!this.state.modalIssue } />
            </div>
        );
    }

    private goToPage(page: number) {
        const from = this.props.match.params.from || '';
        const to = this.props.match.params.to || '';
        if(this.props.match.path.startsWith('/admin')) {
            this.props.history.push(`/admin/${from}${to}${page + 1}`);
        } else {
            this.props.history.push(`/${from}${to}${page + 1}`);
        }
    }

    private fromDateChanged(time: any) {
        this.setState({
            fromDate: time,
        });
    }

    private toDateChanged(time: any) {
        let fromDate = this.state.fromDate as any;
        if(fromDate && fromDate.diff(time) > -24 * 60 * 60 * 1000) {
            fromDate = moment(time).subtract(1, 'day');
        }
        this.setState({
            fromDate,
            toDate: time,
        });
    }

    private applyFilter(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        const from = this.state.fromDate ? `from:${+this.state.fromDate.utc().toDate()}/` : '';
        const to = this.state.toDate ? `to:${+this.state.toDate.utc().toDate()}/` : '';
        if(this.props.match.path.startsWith('/admin')) {
            this.props.history.push(`/admin/${from}${to}1`);
        } else {
            this.props.history.push(`/${from}${to}1`);
        }
    }

    private clearFilter(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        if(this.props.match.path.startsWith('/admin')) {
            this.props.history.push(`/admin/1`);
        } else {
            this.props.history.push(`/1`);
        }
    }

    private onShowModal(issue: SmallComputerIssue) {
        this.setState({
            modalIssue: issue,
        });
    }

    private onDiscardModalDismiss() {
        this.setState({
            modalIssue: null,
        });
    }

    private onDiscardModalDoDiscard(text: string) {
        this.waitingForDiscardUpdate = true;
        this.props.discardIssue(this.state.modalIssue!, text);
    }

    private get fromDate() {
        const { from } = this.props.match.params;
        if(from) {
            const num = Number(from.substring(5, from.length - 1));
            if(!isNaN(num)) {
                return moment.utc(num).local();
            }
        }
        return undefined;
    }

    private get toDate() {
        const { to } = this.props.match.params;
        if(to) {
            const num = Number(to.substring(3, to.length - 1));
            if(!isNaN(num)) {
                return moment.utc(num).local();
            }
        }
        return undefined;
    }

    private get page() {
        const { page } = this.props.match.params;
        if(isNaN(Number(page)) || Number(page) < 1) {
            if(this.props.match.path.startsWith('/admin')) {
                this.props.history.push('/admin/1');
            } else {
                this.props.history.push('/1');
            }
            return 0;
        }
        return Number(page) - 1;
    }

}

export const IssuesListPage = translate()(IssuesListPageClass);