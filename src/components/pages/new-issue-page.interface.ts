import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import { InvalidRequestError, NewComputerIssue, ServerUnavailableError } from 'src/common/lib/api';
import { ErrorsState } from 'src/common/redux/errors/state';
import { User } from 'src/common/redux/user/state';

export interface NewIssuePageStateToProps {
    invalidError: InvalidRequestError | null;
    issueNum: number | null;
    updating: boolean;
    user: User;
    serverError: ServerUnavailableError | null;
}

export interface NewIssuePageDispatchToProps {
    create: (issue: NewComputerIssue) => void;
    cleanUpdateStatus: () => void;
    clearError: (what?: keyof ErrorsState) => void;
}

export type NewIssuePageProps = NewIssuePageStateToProps & NewIssuePageDispatchToProps & InjectedTranslateProps &
    RouteComponentProps<{}>;