import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import { NotFoundError, ServerUnavailableError, SmallComputerIssue } from 'src/common/lib/api';
import { ErrorsState } from 'src/common/redux/errors/state';
import { User } from 'src/common/redux/user/state';

export interface IssuesListPageStateToProps {
    loading: boolean;
    totalPages?: number;
    issues?: SmallComputerIssue[];
    currentPage?: number;
    updating: boolean;
    user: User;
    notFoundError: NotFoundError | null;
    serverError: ServerUnavailableError | null;
}

export interface IssuesListPageDispatchToProps {
    clearError: (what?: keyof ErrorsState) => void;
    clearUpdating: () => void;
    loadPage: (page: number, fromDate?: Date, toDate?: Date) => void;
    reloadList: (fromDate?: Date, toDate?: Date) => void;
    goToPage: (page: number) => void;
    unload: () => void;
    discardIssue: (issue: SmallComputerIssue, reason: string) => void;
}

export type IssuesListPageOwnProps = RouteComponentProps<{ page: string; from?: string; to?: string; }>;

export type IssuesListPageProps = IssuesListPageStateToProps & IssuesListPageDispatchToProps & IssuesListPageOwnProps
    & InjectedTranslateProps;