import React from 'react';
import { Trans, translate } from 'react-i18next';
import { Redirect } from 'react-router';

import { Button } from 'src/common/components/bootstrap/buttons';
import { ComputerIssueAvailability, ComputerIssueAvailabilityRange, NewComputerIssue } from 'src/common/lib/api';
import { OnceTimer } from 'src/common/lib/once-timer';
import { AvailabilityChart } from 'src/components/availability-chart';
import { NewIssuePageProps } from 'src/components/pages/new-issue-page.interface';

interface NewIssuePageState {
    issue: NewComputerIssue;
    typeInvalid: boolean;
    descriptionInvalid: boolean;
    availabilityInvalid: boolean;
    redirect: boolean;
}

class NewIssuePageClass extends React.Component<NewIssuePageProps, NewIssuePageState> {

    private clearErrorTimer = new OnceTimer(() => this.props.clearError());

    constructor(props: NewIssuePageProps) {
        super(props);

        this.state = {
            availabilityInvalid: false,
            descriptionInvalid: false,
            issue: {
                availability_json: {
                    fri: [],
                    mon: [],
                    sat: [],
                    sun: [],
                    thu: [],
                    tue: [],
                    wed: [],
                },
                is_other: false,
                is_wifi: false,
                is_wire: false,
                issue_description: '',
                usuario_niu: this.props.user.niu, //DON'T MODIFY IT :)
            },
            redirect: false,
            typeInvalid: false,
        };

        this.onAvailabilityHourRangeClicked = this.onAvailabilityHourRangeClicked.bind(this);
        this.onAvailabilityDayClicked = this.onAvailabilityDayClicked.bind(this);
        this.onAvailabilityChanged = this.onAvailabilityChanged.bind(this);
        this.onOtherOptionChanged = this.onOtherOptionChanged.bind(this);
        this.onDescriptionChanged = this.onDescriptionChanged.bind(this);
        this.onWifiOptionChanged = this.onWifiOptionChanged.bind(this);
        this.onWireOptionChanged = this.onWireOptionChanged.bind(this);
        this.create = this.create.bind(this);
        this.clean = this.clean.bind(this);
    }

    public componentDidUpdate(prevProps: Readonly<NewIssuePageProps>) {
        if(prevProps.updating && !this.props.updating && !this.props.invalidError && !this.props.serverError) {
            this.setState({ redirect: true });
        } else if(!prevProps.invalidError && this.props.invalidError) {
            this.props.cleanUpdateStatus();
            this.clearErrorTimer.setTimeout(5000);
        } else if(!prevProps.serverError && this.props.serverError) {
            if(this.props.updating) {
                this.props.cleanUpdateStatus();
            }
            setTimeout(() => this.props.clearError('serverUnavailable'));
        }
    }

    public render() {
        const { updating, t } = this.props;
        const { availabilityInvalid, descriptionInvalid, issue, redirect, typeInvalid } = this.state;

        if(redirect) {
            return <Redirect to={ `/issue/${this.props.issueNum}` } />;
        }

        return (
            <div>
                <h1 className="display-4">{ t('new.title') }</h1>
                <form>
                    <fieldset disabled={ updating }>
                    <div className="mb-2">
                        <div>{ t('new.type_title') }</div>
                        <div className="form-check form-check-inline">
                            <input type="checkbox"
                                   className={ `form-check-input ${ typeInvalid ? 'is-invalid' : '' }` }
                                   id="is-wifi-checkbox"
                                   onChange={ this.onWifiOptionChanged }
                                   checked={ issue.is_wifi } />
                            <label htmlFor="is-wifi-checkbox" className="form-check-label">{ t('issue.is_wifi') }</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input type="checkbox"
                                   className={ `form-check-input ${ typeInvalid ? 'is-invalid' : '' }` }
                                   id="is-wire-checkbox"
                                   onChange={ this.onWireOptionChanged }
                                   checked={ issue.is_wire } />
                            <label htmlFor="is-wire-checkbox" className="form-check-label">{ t('issue.is_wire') }</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input type="checkbox"
                                   className={ `form-check-input ${ typeInvalid ? 'is-invalid' : '' }` }
                                   id="is-other-checkbox"
                                   onChange={ this.onOtherOptionChanged }
                                   checked={ issue.is_other } />
                            <label htmlFor="is-other-checkbox" className="form-check-label">{ t('issue.is_other') }</label>
                        </div>
                        <small className="form-text text-muted">{ t('new.type_help') }</small>
                        <small className="form-text text-muted">{ t('new.type_help1') }</small>
                        <small className="form-text text-muted">{ t('new.type_help2') }</small>
                        <small className="form-text text-muted">{ t('new.type_help3') }</small>
                    </div>

                    <div className="form-group">
                        <label htmlFor="description-textarea">{ t('new.description_title') }</label>
                        <textarea className={ `form-control ${descriptionInvalid ? 'is-invalid' : ''}` }
                                  id="description-textarea"
                                  minLength={ 20 }
                                  maxLength={ 1000 }
                                  rows={ 7 }
                                  onChange={ this.onDescriptionChanged }
                                  value={ issue.issue_description } />
                        <small className="form-text text-muted">
                            <Trans i18nKey="new.description_count" count={ 10000 - issue.issue_description.length }>
                                <span className={ issue.issue_description.length >= 9975 ? 'text-warning' : '' }>{{
                                    count: 1000 - issue.issue_description.length
                                }}</span>
                            </Trans>
                            &nbsp;
                            <Trans i18nKey="new.description_help">
                                {{ min: 25 }}
                                {{ max: 10000 }}
                            </Trans>
                        </small>
                        <small className="form-text text-muted">{ t('new.description_help_again') }</small>
                    </div>
                    </fieldset>

                    <div className="mb-4">
                        <div>{ t('new.availability_title') }</div>
                        <AvailabilityChart availability={ issue.availability_json }
                                           onChange={ this.onAvailabilityChanged }
                                           onDayClicked={ this.onAvailabilityDayClicked }
                                           onHourRangeClicked={ this.onAvailabilityHourRangeClicked } />
                        <small className="form-text text-muted">{ t('new.availability_help') }</small>
                        { availabilityInvalid && <div className="invalid-feedback d-block">{ t('new.availability_invalid') }</div> }
                    </div>

                    { this.props.invalidError && <div className="alert alert-danger">{ t(this.props.invalidError.translatedMessageKey) }</div> }

                    <Button type="primary"
                            className="mr-1"
                            onClick={ this.create }
                            disabled={ this.formIsInvalid || updating }>
                        { updating ? t('new.creating') : t('new.create') }
                    </Button>
                    <Button type="secondary" disabled={ updating } onClick={ this.clean }>{ t('new.clean') }</Button>
                </form>
            </div>
        );
    }

    private onWifiOptionChanged() {
        const { is_wifi, is_wire, is_other } = this.state.issue;
        this.setState({
            issue: {
                ...this.state.issue,
                is_wifi: !is_wifi,
            },
            typeInvalid: is_wifi && !is_wire && !is_other,
        });
    }

    private onWireOptionChanged() {
        const { is_wifi, is_wire, is_other } = this.state.issue;
        this.setState({
            issue: {
                ...this.state.issue,
                is_wire: !is_wire,
            },
            typeInvalid: !is_wifi && is_wire && !is_other,
        });
    }

    private onOtherOptionChanged() {
        const { is_wifi, is_wire, is_other } = this.state.issue;
        this.setState({
            issue: {
                ...this.state.issue,
                is_other: !is_other,
            },
            typeInvalid: !is_wifi && !is_wire && is_other,
        });
    }

    private onDescriptionChanged(e: React.ChangeEvent<HTMLTextAreaElement>) {
        this.setState({
            descriptionInvalid: e.target.value.length <= 25 || e.target.value.length > 1000,
            issue: {
                ...this.state.issue,
                issue_description: e.target.value,
            },
        });
    }

    private onAvailabilityChanged(day: keyof ComputerIssueAvailability, range: ComputerIssueAvailabilityRange | null, position?: number) {
        const availability = { ...this.state.issue.availability_json };
        if(position !== undefined) { //Element exists
            if(range) { //Element should be changed
                availability[day][position] = range;
            } else { //Element should be removed
                availability[day] = [
                    ...availability[day].slice(0, position),
                    ...availability[day].slice(position + 1),
                ];
            }
        } else { //Element doesn't exist (range must be valid)
            availability[day] = [
                ...availability[day],
                range!,
            ];
        }

        this.setState({
            availabilityInvalid: Object.keys(availability).reduce((a, key) => a + availability[key].length, 0) < 2,
            issue: {
                ...this.state.issue,
                availability_json: availability,
            },
        });
    }

    private onAvailabilityDayClicked(day: keyof ComputerIssueAvailability, fill: ComputerIssueAvailabilityRange[]) {
        const availability = {
            ...this.state.issue.availability_json,
            [day]: fill,
        };
        this.setState({
            availabilityInvalid: Object.keys(availability).reduce((a, key) => a + availability[key].length, 0) < 2,
            issue: {
                ...this.state.issue,
                availability_json: availability,
            },
        });
    }

    private onAvailabilityHourRangeClicked(positions: Array<number | null>, range: ComputerIssueAvailabilityRange | null) {
        const availability = { ...this.state.issue.availability_json };
        for(const [key, i] of [ ['mon', 0], ['tue', 1], ['wed', 2], ['thu', 3], ['fri', 4], ['sat', 5], ['sun', 6] ]) {
            if(positions[i] !== null) {
                if(range) {
                    availability[key][positions[i]] = range;
                } else {
                    availability[key] = [
                        ...availability[key].slice(0, positions[i]),
                        ...availability[key].slice(positions[i] + 1),
                    ];
                }
            } else if(range) {
                availability[key] = availability[key].concat(range);
            }
        }

        this.setState({
            availabilityInvalid: Object.keys(availability).reduce((a, key) => a + availability[key].length, 0) < 2,
            issue: {
                ...this.state.issue,
                availability_json: availability,
            },
        });
    }

    private get formIsInvalid(): boolean {
        const { availabilityInvalid, descriptionInvalid, issue, typeInvalid } = this.state;
        if(!availabilityInvalid && !descriptionInvalid && !typeInvalid) {
            return (!issue.is_other && !issue.is_wire && !issue.is_wifi) ||
                (25 > issue.issue_description.length || issue.issue_description.length > 1000) ||
                Object.keys(issue.availability_json).reduce((a, key) => a + issue.availability_json[key].length, 0) < 2;
        } else {
            return availabilityInvalid || descriptionInvalid || typeInvalid;
        }
    }

    private clean(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.setState({
            availabilityInvalid: false,
            descriptionInvalid: false,
            issue: {
                availability_json: {
                    fri: [],
                    mon: [],
                    sat: [],
                    sun: [],
                    thu: [],
                    tue: [],
                    wed: [],
                },
                is_other: false,
                is_wifi: false,
                is_wire: false,
                issue_description: '',
                usuario_niu: this.props.user.niu, //DON'T MODIFY IT :)
            },
            typeInvalid: false,
        });
    }

    private create(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.create(this.state.issue);
    }

}

export const NewIssuePage = translate()(NewIssuePageClass);
