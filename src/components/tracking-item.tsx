import React from 'react';
import { InjectedTranslateProps, translate } from 'react-i18next';

import { ComputerIssueState, ComputerIssueTracking } from 'src/common/lib/api';
import { ComputerIssueStateBadge } from 'src/components/computer-issue-state-badge';

const moment = require('moment');

interface TrackingItemProps extends InjectedTranslateProps {
    nextState: ComputerIssueState;
    item: ComputerIssueTracking;
}

class TrackingItemClass extends React.Component<TrackingItemProps> {

    public render() {
        const { item, nextState, t } = this.props;

        let badges;
        if(item.from_state !== nextState) {
            badges = <div className="col-auto align-items-center" style={{ overflowX: 'scroll', maxWidth: '100%' }}>
                <div style={{ whiteSpace: 'nowrap' }}>
                <ComputerIssueStateBadge state={ item.from_state } t={ t } className="mr-1" />
                →
                <ComputerIssueStateBadge state={ nextState } t={ t } className="ml-1" />
                </div>
            </div>;
        } else {
            badges = <div className="col-auto align-items-center"><ComputerIssueStateBadge state={ item.from_state } t={ t } /></div>;
        }

        return (
            <div className="col-12">
                <div className="card tracking-item mt-3">
                    <div className="card-header">
                        <div className="row justify-content-between">
                            { badges }
                            <div className="col-auto"><span className="text-muted">{ moment.utc(item.date_time).format('lll') }</span></div>
                        </div>
                    </div>
                    <div className="card-body"><p className="card-text">{ item.message }</p></div>
                </div>
            </div>
        );
    }

}

export const TrackingItem = translate()(TrackingItemClass);
