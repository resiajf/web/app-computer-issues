import React from 'react';
import { InjectedTranslateProps } from 'react-i18next';

import { Badge } from 'src/common/components/bootstrap/badge';
import { ComputerIssueState } from 'src/common/lib/api';

interface Props extends InjectedTranslateProps {
    state: ComputerIssueState;
    className?: string;
}

export const ComputerIssueStateBadge = ({ className, state, t }: Props) => {
    let type: string = 'secondary';
    switch(state) {
        case 'pending':      type = 'secondary'; break;
        case 'discarded':    type = 'secondary'; break;
        case 'solved':       type = 'success'; break;
        case 'waiting':      type = 'primary'; break;
        case 'waiting-user': type = 'warning'; break;
        case 'wont-fix':     type = 'danger'; break;
        case 'working':      type = 'info'; break;
    }

    // @ts-ignore
    return <Badge type={ type } pill={ true } className={ className }>{ t(`issue.state.${state}`) }</Badge>;
};