import React from 'react';

import { ComputerIssueAvailability, ComputerIssueAvailabilityRange } from 'src/common/lib/api';

const moment = require('moment');

interface AvailabilityChartProps {
    availability: ComputerIssueAvailability;
    onChange?: (day: keyof ComputerIssueAvailability, range: ComputerIssueAvailabilityRange | null, position?: number) => void;
    onDayClicked?: (day: keyof ComputerIssueAvailability, fill: ComputerIssueAvailabilityRange[]) => void;
    onHourRangeClicked?: (positions: Array<number | null>, range: ComputerIssueAvailabilityRange | null) => void;
}

const hoursAvailable = [ 10, 11, 12, 13, 16, 17, 18, 19, 20 ];

export class AvailabilityChart extends React.Component<AvailabilityChartProps> {

    public render() {
        return (
            <div className={ `availability-chart${this.props.onChange ? ' availability-chart-editable' : ''}` }>
                <div className="availability-chart-container">
                    <div className="row">
                        <div className="col-1" />
                        <div className="col-1" onClick={ this.onDayClick('mon') }>{ moment().day(1).format('dddd') }</div>
                        <div className="col-1" onClick={ this.onDayClick('tue') }>{ moment().day(2).format('dddd') }</div>
                        <div className="col-1" onClick={ this.onDayClick('wed') }>{ moment().day(3).format('dddd') }</div>
                        <div className="col-1" onClick={ this.onDayClick('thu') }>{ moment().day(4).format('dddd') }</div>
                        <div className="col-1" onClick={ this.onDayClick('fri') }>{ moment().day(5).format('dddd') }</div>
                        <div className="col-1" onClick={ this.onDayClick('sat') }>{ moment().day(6).format('dddd') }</div>
                        <div className="col-1" onClick={ this.onDayClick('sun') }>{ moment().day(0).format('dddd') }</div>
                    </div>
                    { hoursAvailable.map((hour) => (
                        <div className="row" key={ hour }>
                            <div className="col-1" onClick={ this.onHourRangeClick(hour) }>{ hour }-{ hour + 1 }</div>
                            { this.cell(hour, 'mon') }
                            { this.cell(hour, 'tue') }
                            { this.cell(hour, 'wed') }
                            { this.cell(hour, 'thu') }
                            { this.cell(hour, 'fri') }
                            { this.cell(hour, 'sat') }
                            { this.cell(hour, 'sun') }
                        </div>
                    )) }
                </div>
            </div>
        );
    }

    private findRange(hour: number, day: keyof ComputerIssueAvailability): [ ComputerIssueAvailabilityRange | null, number ] {
        const availability = this.props.availability[day];
        const i = availability.findIndex((range) => (moment(range.from, 'hh:mm').hour() <= hour && hour < moment(range.to, 'hh:mm').hour()));
        return [ i !== -1 ? availability[i] : null, i ];
    }

    private cellShouldBeEnabled(hour: number, day: keyof ComputerIssueAvailability) {
        const [ item ] = this.findRange(hour, day);
        if(item) {
            return item.maybe ? 'maybe' : 'yes';
        }
        return 'no';
    }

    private cell(hour: number, day: keyof ComputerIssueAvailability) {
        const type = this.cellShouldBeEnabled(hour, day);
        if(type === 'yes') {
            //Icon from: https://iconmonstr.com/check-mark-1-svg/
            return <div className="col-1 yes" onClick={ this.onCellClick(hour, day) }>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/>
                </svg>
            </div>;
        } else if(type === 'maybe') {
            //Icon from: https://iconmonstr.com/help-1-svg/
            return <div className="col-1 maybe" onClick={ this.onCellClick(hour, day) }>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M14.601 21.5c0 1.38-1.116 2.5-2.499 2.5-1.378 0-2.499-1.12-2.499-2.5s1.121-2.5 2.499-2.5c1.383 0 2.499 1.119 2.499 2.5zm-2.42-21.5c-4.029 0-7.06 2.693-7.06 8h3.955c0-2.304.906-4.189 3.024-4.189 1.247 0 2.57.828 2.684 2.411.123 1.666-.767 2.511-1.892 3.582-2.924 2.78-2.816 4.049-2.816 7.196h3.943c0-1.452-.157-2.508 1.838-4.659 1.331-1.436 2.986-3.222 3.021-5.943.047-3.963-2.751-6.398-6.697-6.398z"/>
                </svg>
            </div>;
        } else {
            //Icon from: https://iconmonstr.com/x-mark-1-svg/
            return <div className="col-1 no" onClick={ this.onCellClick(hour, day) }>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/>
                </svg>
            </div>;
        }
    }

    private onCellClick(hour: number, day: keyof ComputerIssueAvailability) {
        if(!this.props.onChange) {
            return undefined;
        }

        //NOTE: This supposes that every range is from X:00 to (X+1):00. That kind of ranges should be used when
        //      editing the availability, not when showing the final result.
        return (e: React.MouseEvent<HTMLDivElement>) => {
            e.preventDefault();
            const [ item, pos ] = this.findRange(hour, day);
            if(item) {
                if(item['maybe']) {
                    this.props.onChange!(day, null, pos!);
                } else {
                    this.props.onChange!(day, { from: `${hour}:00`, to: `${hour + 1}:00`, maybe: true }, pos!);
                }
            } else {
                this.props.onChange!(day, { from: `${hour}:00`, to: `${hour + 1}:00` });
            }
        };
    }

    private onDayClick(day: keyof ComputerIssueAvailability) {
        if(!this.props.onDayClicked) {
            return undefined;
        }

        return () => {
            if(this.props.availability[day].length !== hoursAvailable.length) {
                this.props.onDayClicked!(day, hoursAvailable.map((hour) => ({ from: `${hour}:00`, to: `${hour + 1}:00` })));
            } else {
                const maybes = new Set(this.props.availability[day].map((item) => Boolean(item!.maybe)));
                if(maybes.size === 1 && maybes.values().next().value) {
                    this.props.onDayClicked!(day, []);
                } else {
                    this.props.onDayClicked!(day, hoursAvailable.map((hour) => ({ from: `${hour}:00`, to: `${hour + 1}:00`, maybe: true })));
                }
            }
        };
    }

    private onHourRangeClick(hour: number) {
        if(!this.props.onHourRangeClicked) {
            return undefined;
        }

        return () => {
            const pos = [
                this.findRange(hour, 'mon'),
                this.findRange(hour, 'tue'),
                this.findRange(hour, 'wed'),
                this.findRange(hour, 'thu'),
                this.findRange(hour, 'fri'),
                this.findRange(hour, 'sat'),
                this.findRange(hour, 'sun'),
            ];

            if(pos.filter(([_, i]) => i !== -1).length === 7 /* days */) {
                const maybes = new Set(pos.map(([item]) => Boolean(item!.maybe)));
                if(maybes.size === 1 && maybes.values().next().value) {
                    this.props.onHourRangeClicked!(pos.map(([ _, i ]) => i === -1 ? null : i), null);
                } else {
                    this.props.onHourRangeClicked!(pos.map(([_, i]) => i === -1 ? null : i), { from: `${hour}:00`, to: `${hour + 1}:00`, maybe: true });
                }
            } else {
                this.props.onHourRangeClicked!(pos.map(([_, i]) => i === -1 ? null : i), { from: `${hour}:00`, to: `${hour + 1}:00` });
            }
        };
    }

}