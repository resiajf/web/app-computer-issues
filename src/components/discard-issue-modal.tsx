import React from 'react';
import { InjectedTranslateProps, Trans, translate } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'src/common/components/bootstrap/modal';

type DiscardIssueModalProps = InjectedTranslateProps & {
    onDiscard: (text: string) => void,
    onDismiss: () => void,
    show: boolean,
};

interface DiscardIssueModalState {
    why: string;
}

class DiscardIssueModalClass extends React.Component<DiscardIssueModalProps, DiscardIssueModalState> {

    constructor(props: DiscardIssueModalProps) {
        super(props);

        this.state = {
            why: ''
        };

        this.whyChanged = this.whyChanged.bind(this);
        this.dismiss = this.dismiss.bind(this);
        this.close = this.close.bind(this);
    }


    public render() {
        const { t, show } = this.props;
        const { why } = this.state;
        return (
            <Modal show={ show } onClosed={ this.close }>
                <ModalHeader title={ t('issue.discard.title') } onClose={ this.close } />
                <ModalBody>
                    <p>{ t('issue.discard.explanation_text') }</p>
                    <textarea className="form-control"
                              rows={ 5 }
                              minLength={ 10 }
                              maxLength={ 255 }
                              onChange={ this.whyChanged }
                              value={ why } />
                    <small className="form-text text-muted">
                        <Trans i18nKey="issue.discard.textarea_help_text" count={ 250 - why.length }>
                            <span className={ 250 - why.length <= 25 ? 'text-warning' : (why.length < 10 ? 'text-info' : '') }>{{count: 250-why.length}}</span> characters.
                            /
                            The maximum characters allowed are {{ limit: 250 }},
                            the minimum required are {{ minim: 10 }} instead.
                        </Trans>
                    </small>
                </ModalBody>
                <ModalFooter>
                    <Button type="secondary" onClick={ this.close }>
                        { t('issue.discard.close') }
                    </Button>
                    <Button type="danger" onClick={ this.dismiss } disabled={ why.length <= 10 }>
                        { t('issue.discard.discard') }
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }

    private whyChanged(e: React.ChangeEvent<HTMLTextAreaElement>) {
        e.preventDefault();
        this.setState({
            why: e.target.value,
        });
    }

    private close(e?: React.MouseEvent<HTMLButtonElement>) {
        if(e) {
            e.preventDefault();
        }
        this.props.onDismiss();
    }

    private dismiss(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.onDiscard(this.state.why);
    }

}

export const DiscardIssueModal = translate()(DiscardIssueModalClass);