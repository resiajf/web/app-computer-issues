import React from 'react';
import { InjectedTranslateProps, Trans, translate } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';
import { ComputerIssueState } from 'src/common/lib/api';

interface ChangeStateFormProps extends InjectedTranslateProps {
    onUpdateStatus: (reason: string, state?: ComputerIssueState) => void;
    updating: boolean;
    forceStatusChangeTo?: ComputerIssueState;
}

interface ChangeStateFormState {
    updateText: string;
    updateState: ComputerIssueState | 'no-change';
}

class ChangeStateFormClass extends React.Component<ChangeStateFormProps, ChangeStateFormState> {

    private isUpdatingStatus: boolean = false;

    constructor(props: ChangeStateFormProps) {
        super(props);

        this.state = {
            updateState: props.forceStatusChangeTo || 'no-change',
            updateText: '',
        };

        this.onTextAreaChange = this.onTextAreaChange.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.onClearClicked = this.onClearClicked.bind(this);
        this.onUpdateStatus = this.onUpdateStatus.bind(this);
    }

    public componentDidUpdate(prevProps: Readonly<ChangeStateFormProps>) {
        if(prevProps.updating && !this.props.updating) {
            //TODO handle errors
            if(this.isUpdatingStatus) {
                this.isUpdatingStatus = false;
                this.setState({
                    updateState: 'no-change',
                    updateText: ''
                });
            }
        }
    }

    public render() {
        const { forceStatusChangeTo, t } = this.props;
        const { updateText, updateState } = this.state;
        const updating = this.props.updating && this.isUpdatingStatus;

        return (
            <div className="mb-4">
                <h2>{ t('item.update_tracking_title') }</h2>
                { !forceStatusChangeTo && <select className="form-control mb-2" value={ updateState } onChange={ this.onSelectChange } disabled={ updating }>
                    <option value={ 'no-change' }>{ t('item.update_tracking_no_change') }</option>
                    <option value={ 'waiting' }>{ t('issue.state.waiting') }</option>
                    <option value={ 'waiting-user' }>{ t('issue.state.waiting-user') }</option>
                    <option value={ 'working' }>{ t('issue.state.working') }</option>
                    <option value={ 'wont-fix' }>{ t('issue.state.wont-fix') }</option>
                    <option value={ 'solved' }>{ t('issue.state.solved') }</option>
                </select> }
                <textarea className="form-control"
                          disabled={ updating }
                          rows={ 5 }
                          onChange={ this.onTextAreaChange }
                          value={ this.state.updateText } />
                <small className="form-text text-muted">
                    <Trans i18nKey="issue.discard.textarea_help_text" count={ 250 - updateText.length }>
                        <span className={ 250 - updateText.length <= 25 ? 'text-warning' : (updateText.length < 10 ? 'text-info' : '') }>{{count: 250-updateText.length}}</span> characters.
                        /
                        The maximum characters allowed are {{ limit: 250 }},
                        the minimum required are {{ minim: 10 }} instead.
                    </Trans>
                </small>
                <div className="mt-2 text-right">
                    <Button type="primary" disabled={ updateText.length < 11 || updating } onClick={ this.onUpdateStatus }>
                        { t('item.update_tracking_send') }
                    </Button>
                    <Button type="secondary" className="ml-1" disabled={ updating } onClick={ this.onClearClicked }>
                        { t('item.update_tracking_clear') }
                    </Button>
                </div>
            </div>
        );
    }

    private onTextAreaChange(e: React.ChangeEvent<HTMLTextAreaElement>) {
        this.setState({
            updateText: e.target.value,
        });
    }

    private onSelectChange(e: React.ChangeEvent<HTMLSelectElement>) {
        this.setState({
            updateState: e.target.value as any
        });
    }

    private onClearClicked(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.setState({
            updateState: 'no-change',
            updateText: '',
        });
    }

    private onUpdateStatus(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.isUpdatingStatus = true;
        this.props.onUpdateStatus(
            this.state.updateText,
            this.state.updateState === 'no-change' ? undefined : this.state.updateState
        );
    }

}

export default translate()(ChangeStateFormClass);