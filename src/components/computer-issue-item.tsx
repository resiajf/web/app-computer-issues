import React from 'react';
import { InjectedTranslateProps, Trans, translate } from 'react-i18next';
import { Link } from 'react-router-dom';

import { Badge } from 'src/common/components/bootstrap/badge';
import { Button } from 'src/common/components/bootstrap/buttons';
import { SmallComputerIssue } from 'src/common/lib/api';
import { ComputerIssueStateBadge } from 'src/components/computer-issue-state-badge';

const moment = require('moment');

const ellipsize = (text: string, num: number = 50) => {
    if(text.length < num) {
        return text;
    } else {
        let textShort = text.substr(0, num);
        textShort = text.substring(0, textShort.lastIndexOf(' '));
        return `${textShort}…`;
    }
};

export default translate()(({ issue, onShowModal, userNiu, t }: { issue: SmallComputerIssue, onShowModal: (issue: SmallComputerIssue) => void, userNiu: number } & InjectedTranslateProps) => {
    const onClick = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        onShowModal(issue);
    };

    return (
        <div className="col-12 mb-3">
            <div className="row">
                <div className="col-auto">
                    <ComputerIssueStateBadge state={ issue.state } t={ t } />
                </div>
                <div className="col" />
                <div className="col-auto">
                    { issue.is_wifi && <Badge type="info">{ t('issue.is_wifi') }</Badge> }
                    { issue.is_wire && <Badge type="info" className="ml-1">{ t('issue.is_wire') }</Badge> }
                    { issue.is_other && <Badge type="info" className="ml-1">{ t('issue.is_other') }</Badge> }
                </div>
            </div>

            <div>
                { ellipsize(issue.issue_description, 75) }
            </div>

            <div className="text-muted small">
                <Trans i18nKey="issue.created_at">
                    created
                    <time className="text-muted" dateTime={issue.fecha_creacion.toString()}>
                        {{ time: moment.utc(issue.fecha_creacion).fromNow(true) }}
                    </time>
                    ago
                </Trans>
            </div>

            <div className="d-flex justify-content-end">
                <Link to={ `/issue/${issue.id_parte_informatica}` } className="btn btn-sm btn-outline-primary mr-2">{ t('issue.go_to_issue') }</Link>
                { issue.state !== 'discarded' && issue.state !== 'solved' && issue.state !== 'wont-fix' && issue.usuario_niu === userNiu &&
                    <Button type="danger" outline={ true } size="sm" onClick={ onClick }>{ t('issue.discard.discard') }</Button> }
            </div>
        </div>
    );
});